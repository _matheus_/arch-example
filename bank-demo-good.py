class CustomerRepository:
    def get_preferred_notification_channels(self, customer):
        return []


class BankStatementRepository:
    def get_customer_bank_statement(self, customer):
        return {}


class NotificationService:
    def send_by_email(self, bank_statement):
        pass

    def send_by_sms(self, bank_statement):
        pass


class EmailNotificationChannel:
    def send(self, bank_statement):
        pass


class SmsNotificationChannel:
    def send(self, bank_statement):
        pass


class NotificationChannelFactory:
    @staticmethod
    def get_notification_channel(channel):
        if channel == 'email':
            return EmailNotificationChannel()
        elif channel == 'sms':
            return SmsNotificationChannel()


class BankStatementSend:
    def __init__(self, customer_repository, ):
        pass


def send_bank_statement(customer):
    customer_repository = CustomerRepository()
    preferred_channels = customer_repository.get_preferred_notification_channel(
        customer)
    bank_statement_repository = BankStatementRepository()
    bank_statement = bank_statement_repository.get_customer_bank_statement(
        customer)

    for channel in preferred_channels:
        NotificationChannelFactory.get_notification_channel(
            channel).send(bank_statement)
