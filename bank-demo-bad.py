class CustomerRepository:
    def get_preferred_notification_channels(self, customer):
        return []


class BankStatementRepository:
    def get_customer_bank_statement(self, customer):
        return {}


class NotificationService:
    def send_by_email(self, bank_statement):
        pass

    def send_by_sms(self, bank_statement):
        pass


def send_bank_statement(customer):
    notification_service = NotificationService()
    customer_repository = CustomerRepository()
    preferred_channels = customer_repository.get_preferred_notification_channel(
        customer)
    bank_statement_repository = BankStatementRepository()
    bank_statement = bank_statement_repository.get_customer_bank_statement(
        customer)

    for channel in preferred_channels:
        if channel == 'email':
            notification_service.send_by_email(bank_statement)
        elif channel == 'sms':
            notification_service.send_by_sms(bank_statement)
